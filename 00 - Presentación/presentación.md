# Presentación
El módulo Bastionado de Redes y Sistemas tiene una duración de 95 horas.  Este módulo profesional contiene la formación necesaria para desempeñar la
función de bastionado de los sistemas y redes de la organización.

La función de bastionado incluye aspectos como la **administración de los sistemas y redes** contemplando la normativa, tanto a nivel nacional como internacional, de ciberseguridad en vigor.

Las actividades profesionales asociadas a esta función **se aplican en el diseño de planes de securización y en el diseño de las redes contemplando los requisitos de seguridad** que apliquen a la organización.

Entre otros, este módulo ayuda a alcanzar los siguientes objetivos:

- Diseñar e implantar planes de medidas técnicas de seguridad a partir de los riesgos identificados para garantizar el nivel de seguridad requerido.
- Configurar sistemas de control de acceso, autenticación de personas y
administración de credenciales para preservar la privacidad de los datos.
- Configurar la seguridad de sistemas informáticos para minimizar las probabilidades de exposición a ataques.
- Configurar dispositivos de red para cumplir con los requisitos de seguridad.
- Administrar la seguridad de sistemas informáticos en red aplicando las políticas de seguridad requeridas para garantizar la funcionalidad necesaria con el nivel de riesgo de red controlado.

Las líneas de actuación en el proceso de enseñanza aprendizaje que permiten alcanzar los objetivos del módulo versarán sobre:

- El diseño de planes de securización de la organización.
- El diseño de redes de computadores.
- La administración de los sistemas de control de acceso.

## Unidades de trabajo
Los contenidos se dividirán en las siguientes unidades de trabajo u otras similares. El orden de las unidades cambiará para adaptarse a la forma de trabajo durante el curso.

- Diseño de planes de securización
- Sistemas de control de acceso y autenticación de personas (mecanismos de autenticación)
- Administración de credenciales de acceso (gestión de credenciales/protocolos de acceso)
- Diseño de redes de computadoras seguras (redes)
- Configuración de dispositivos y sistemas informáticos (seguridad perimetral)
- Configuración de dispositivos para la instalación de sistemas informáticos. (Instalación de PC y servidor)
- Configuración de los sistemas informáticos. (Seguridad en el puesto de trabajo y el servidor)

## Plan de trabajo
En el siguiente esquema de red disponéis de un resumen de las partes de la red con que vamos a trabajar en las diferentes unidades de trabajo:

![Mapa de la red](recursos/mapa-red.png)

El documento completo, sobre el que trabajaremos a lo largo del curso se encuentra en este enlace:

https://moodle.educarex.es/iescastelarfp/mod/url/view.php?id=25743

Organizaremos el trabajo a lo largo del curso centrándonos en cada una de las partes del diagrama de red. El objetivo será montar la red de una empresa ficticia con diversos servicios de red y con diversas medidas de seguridad.

## Equipos de trabajo
El trabajo lo vamos a organizar en grupo durante la mayor parte del curso. 

Intentaremos que los grupos sean de 3-4 alumnos. Será ideal que los alumnos en cada grupo se sienten unos junto a otros para facilitar este planteamiento.


## Software necesario para empezar
En la sección programas del curso disponéis de enlaces para instalar diversas aplicaciones. Aunque durante el curso instalaremos más herramientas y programas, hay algunos que posiblemente vamos a utilizar durante todo el curso. Los siguientes los vamos a tener instalados desde el principio:

### Navegador
- [Firefox](https://moodle.educarex.es/iescastelarfp/mod/url/view.php?id=29564)
- [Plugin markdown](https://moodle.educarex.es/iescastelarfp/mod/url/view.php?id=29565)

### Analizadores de protocolos
- [Wireshark](https://moodle.educarex.es/iescastelarfp/mod/url/view.php?id=29566)

### Herramientas de virtualización
- [VMWare Workstation Pro](https://moodle.educarex.es/iescastelarfp/mod/resource/view.php?id=29576)
- [Virtual Box](https://moodle.educarex.es/iescastelarfp/mod/url/view.php?id=29577)
- [Virtual Box Extension Pack](https://moodle.educarex.es/iescastelarfp/mod/url/view.php?id=29578)

### Kali Linux
- [Kali Linux](https://moodle.educarex.es/iescastelarfp/mod/url/view.php?id=29580)





