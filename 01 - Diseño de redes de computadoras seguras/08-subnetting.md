# Subnetting
En este apartado vamos a hablar de la segmentación de redes.

## ¿Qué es una red plana?
Una red construida sobre dispositivos de capa 2  es conocida como «red plana».

Conceptos:
- **Dominio de colisión**: son los puntos de la red en que los mensajes pueden «chocar». Amplia en este enlace https://blogfactores.wordpress.com/2015/07/08/ccna-dominios-de-colision-y-broadcast/
- **Dominio de broadcast**: es una separación lógica dentro de la red de ordenadores en la que los mensajes, normalmente paquetes de capa 3 en el modelo OSI, pueden ser difundidos para que todos los equipos dentro de ese espacio, definido lógicamente, los puedan recibir. Puedes ampliar la información aquí. https://blogfactores.wordpress.com/2015/07/08/ccna-dominios-de-colision-y-broadcast/ 

## ¿Qué es la segmentación de red?
La segmentación de red es una técnica de seguridad que divide una red en distintas subredes más pequeñas, que permiten a los equipos de red compartimentar las subredes y otorgar controles y servicios de seguridad únicos a cada subred.

El proceso de segmentación de red implica dividir una red en diferentes subredes. Una vez la red se ha subdividido en unidades más pequeñas y manejables, se aplican controles a los segmentos individuales compartimentados.

![Segmentos de red](recursos/segmentacion.png)


## Ventajas 
- Permite separar equipos en función de su propósito o características
- Posibilidad de delegar la administración de las subredes
- Optimización del ancho de banda disponible
- Permite tener más control sobre el tráfico de la red
- Facilidad de análisis e intervención
- Mejora la seguridad. Una amplia red plana presenta inevitablemente una gran superficie de ataque y facilita el desplazamiento lateral en su interior.


## Subnetting
Definido de la forma más simple, el término subnetting hace referencia a la subdivisión de una red en varias subredes.


## Zonas de seguridad
Identificar las distintas zonas en función a su nivel de exposición y riesgo potencial. Segmentando la con red, vlans, firewalls tendremos un cerco mas controlable y restringido.

- Zonas de Seguridad para Recursos Internos:
- Zona de estaciones de trabajo
- Zona de Servidores internos: BBDD, Backups
- Zonas diferenciadas para entornos de desarrollo y pruebas
- Zona para el control de la seguridad y monitorización
- Zona de seguridad para sistemas sinconfianza como accesos wifi o acceso a
servidores externos 

