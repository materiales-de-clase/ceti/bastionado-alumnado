# Ataque de envenenamiento ARP
El protocolo ARP permite resolver direcciones MAC a direcciones IP, pero tiene un problema. No existe autenticación de clientes o servidores. Esto facilita ataques tipo envenenamiento de tablas ARP. Este ataque es una técnica de hacking usada para infiltrarse en una red, con el objetivo de husmear los paquetes que pasan por la LAN, modificar el tráfico o incluso provocar una denegación de servicio (DoS).

Para reproducir este ataque se pueden utilizar herramientas como:

- Cain y Abel
- Arpspoof
- Ettercap
- Arpoison

En este ejercicio vamos a intentar reproducir un ataque de este tipo.

## Reproducir el ataque
En esta web dispones de un ejemplo de utilización de arpspoof para reproducir el ataque.

https://www.javatpoint.com/arp-spoofing-using-arpspoof

En esta web se consigue envenenar la cache ARP del router y un cliente para hacer un ataque MiM. Todo el tráfico intercambiado entre el router y el cliente pasa por el equipo del atacante. Descarga arpspoof desde esta web:

https://github.com/alandau/arpspoof

Y reproduce los pasos en la web para tratar de reproducir el ataque. Para ello trabajad en grupos de cuatro alumnos utilizad un portátil para hacer el ataque y otro cómo victima. 

Vas a utilizar 3 portátiles:

- La víctima
- El atacante
- El sniffer. Utilizarás port mirror para enviar todo el tráfico del ataque a este portátil para analizarlo posteriormente.

Como resultado de este apartado deberás aportar una descripción de la configuración del setup y la ejecución del ataque. Además, utilizando la captura de tráfico, describe en qué consiste el ataque. Además, comprueba revisando la tabla arp si el ataque ha tenido éxito. Prueba también a enviar tráfico al router para ver si se envía al router o al atacante.

## Reflexiona y responde
¿Qué importantísimo requisito tiene este ataque para poder ser realizado? O, dicho de otro modo, ¿desde dónde puede ser realizado este ataque?

## Resolución ARP estática
Para paliar los efectos de este tipo de ataques podemos, por ejemplo, utilizar resolución ARP estática al menos para los equipos sensibles de la red. Esto podría incluir ciertos servidores o incluso la puerta de enlace. Incluso en algunos casos, podríamos desactivar arp después de configurar estas tablas para evitar este tipo de ataques. 

En esta actividad, vamos a definir la entrada para el router de internet de forma estática en el PC victima. Después de esto, vamos a intentar realizar de nuevo el ataque. Analiza los resultados.

Compara los resultados con los del apartado anterior. Describe que ha pasado. Comprueba si la tabla ARP ha sido alterada y si cuando envías tráfico al router lo envías al real.

### Ejemplo de inserción de reglas ARP Windows

Obtener los adaptadores
```
> Get-NetAdapter

Name                      InterfaceDescription                    ifIndex Status       MacAddress             LinkSpeed
----                      --------------------                    ------- ------       ----------             ---------
VMware Network Adapte...8 VMware Virtual Ethernet Adapter for ...      22 Up           00-50-56-C0-00-08       100 Mbps
Wi-Fi                     Intel(R) Wi-Fi 6 AX200 160MHz                17 Disconnected B0-A4-60-CF-90-45          0 bps
Ethernet                  Intel(R) Ethernet Controller I225-V           7 Up           D8-5E-D3-29-B0-9A         1 Gbps
```

Para definir la regla estática
```
> New-NetNeighbor -InterfaceIndex 7 -IPAddress '192.168.0.1' -LinkLayerAddress '0000120000ff' -State Permanent
```

Para eliminar la regla
```
Remove-NetNeighbor -InterfaceIndex 9 -IPAddress '192.168.0.1'
```

### Ejemplo de inserción de reglas ARP Linux
Desactiva ARP
```
# ip link set dev eth0 arp off
```

Asigna una entrada ARP estática
```
# arp -s 10.0.0.2 00:0c:29:c0:94:bf
```

### Haciendo las reglas persistentes
En ocasiones estas reglas se pierden al reiniciar el equipo. Para hacer las reglas persistentes podríamos necesitar ejecutar un script que las inserte en cada inicio de sesión. Para concluir la tarea no es necesario que lo hagas pero aquí tienes un par de enlaces a artículos que te indicarán como ejecutar un script en el inicio de un equipo:

- [Windows](https://rdr-it.com/es/gpo-ejecutar-un-script-al-iniciar-la-computadora/)
- [Linux](https://www.redhat.com/sysadmin/systemd-oneshot-service)

## Entrega
- La prática será realizada en grupos de 4.
- Documento apellidos_nombre_nivel3_tarea02.pdf. Solo tendrá que entregarla uno de los alumnos. Dentro del documento tendrán que aparecer los alumnos integrantes del grupo.
- Sigue los pasos y documenta todo con capturas todo el proceso. Si las hay, es importante que remarques la información importante.
