# VLAN
Las VLAN o también conocidas como «Virtual LAN» nos permite crear redes lógicamente independientes dentro de la misma red física, haciendo uso de switches gestionables que soporten VLANs para segmentar adecuadamente la red. Podríamos hablar de una VLAN como una red lógica que se crea por software. Para conectar esta red con el exterior es necesario mapearla/conectarla a uno o varios interfaces físicos.

Aunque hay algunos tipos de VLAN que conectan los equipos de diferentes formas, las más comunes son las **VLAN basadas en puertos** y son esas de las que vamos a hablar en este apartado.

## ¿Qué nos proporciona una VLAN?
En esencia, lo mismo que podemos obtener haciendo segmentación de redes física utilizando en este caso los equipos para hacer una segmentación lógica. Es decir, nos permite:

- **Seguridad**: las VLAN nos permite crear redes lógicamente independientes, por tanto, podremos aislarlas para que solamente tengan conexión a Internet, y denegar el tráfico de una VLAN a otra.
- **Segmentación**: las VLAN nos permite segmentar todos los equipos en diferentes subredes, a cada subred le asignaremos una VLAN diferente. 
- **Flexibilidad**: gracias a las VLAN podremos colocar a los diferentes equipos en una subred o en otra, de manera fácil y rápida, y tener unas políticas de comunicación donde permitiremos o denegaremos el tráfico hacia otras VLANs o hacia Internet.
- **Optimización de la red**: Al tener subredes más pequeñas, en entornos donde tengamos cientos o miles de equipos conectados, contendremos el broadcast en dominios más pequeños, por tanto, el rendimiento de la red será óptimo, sin tener que transmitir los mensajes de broadcast a todos los equipos conectados, lo que haría que el rendimiento de la red baje radicalmente e incluso podría llegar a colapsarse.
- **enlaces trunk**: Por un solo enlace o interfaz físico podemos pasar muchas VLAN. Para ello se etiquetan los paquetes. (trunk)

## Etiquetado de tramas
El problema de enviar tramas de diferentes VLAN por el mismo puerto es que el equipo conectado, generalemte otro equipo de comunicaciones, no podría distinguir entre las tramas de las diferentes VLAN. Para solucionar este problema existen diferentes protocolos que se pueden utilizar en la configuración de los enlaces. En nuestro caso, nos vamos a centrar en el estándar IEEE 802.1Q. En este protocolo los paquetes son alterados al pasar por un enlace entre equipos, lo que vamos a llamar un trunk, para añadir la siguiente información:

- El campo Ethertype vale 0x8100
- Se añaden dos bytes a modo de etiqueta para codificar:
  - 3 bits: Prioridad
  - 1 bit: Canonical Format Indicator (CFI) usado para indicar la presencia de un campo Routing Information Field (RIF)
  - 12 bits: VLAN ID o VID que identifica de forma única la VLAN a la que pertenece la trama ethernet. Con 12 bits podríamos tener un número entre 0 y 4095 pero el rango válido será 1 a 4094. Las VLAN 0 y 4095 no pueden usarse en los puertos del switch ya que están reservadas para otros usos. 

Un paquete que contenga esta información lo consideraremos **etiquetado**, es decir, el switch le pone una **etiqueta** para distingir la vlan a que pertenece. Podemos transmitir por los puertos paquetes etiquetados y sin etiquetar.

# VLAN de rango normal o extendido
En función de su VID vamos a distinguir dos tipos de VLAN:

- VLAN de **rango normal** (1-1005): Serán las que utilizaremos dentro de redes pequeñas y medianas.
- VLAN de **rango extendido** (1006-4094): Posibilitan a los proveedores de servicios ampliar infraestructuras a una cantidad de clientes mayor. Algunas empresas globales podrían ser lo suficientemente grandes como para necesitar los ID de las VLAN de rango extendido. Estos VID soportan menos características que las VLAN de rango normal. 

## Tipos de puertos
Vamos a distinguir dos tipos de puertos en el switch:

- **Puertos de acceso**: Están pensados para conectar directamente las estaciones de trabajo. Tienen asignada una VLAN que, si no tocamos nada será la VLAN por defecto en el switch. Las tramas que salen por un puerto de acceso **no estan etiquetados** y tienen el formato 802.3 original. Podemos configurar cualquier puerto en el switch como un puerto de acceso. Al no pasar tráfico etiquetado, podemos mapear únicamente una VLAN con este tipo de puerto.
- **Puertos Trunk**: Estos puertos los utilizaremos para conectar switches y nos permite extender la VLAN a otros equipos de red. A un puerto trunk podemos asignarle todas las VLAN que necesitemos ya que **las tramas tendrán el formato 802.1Q**, es decir, estarán etiquetadas (tagged). De cualquier modo, generalmente tenemos que mapear una VLAN a los puertos trunk que pasará sin etiquetar. Esta sería la VLAN Nativa. 

## VLAN nativa
La VLAN nativa, va a ser una VLAN que se va a enviar sin etiquetar por los enlaces trunk. Los equipos de red suelen traer por defecto el puerto 1, aunque los fabricantes recomiendan cambiarla por otra. 

## ¿Cómo se comunican diferentes VLAN?
Una VLAN la podemos considerar como un dominio de difusión que está creado en el software de nuestro equipo de red. Los diferentes equipos que estén conectados a la VLAN pueden comunicarse entre ellos pero no pueden comunicarse con equipos que se encuentran en otras VLAN. Es como si cada VLAN fuera un switch independiente. Cuando creamos y configuramos las VLAN en un switch no se pueden comunicar entre ellas incluso estando en el mismo equipo físico, la única forma de que dos VLAN se vean es ascendiendo a nivel de red (L3), esto lo podemos hacer de diferentes formas:

- Conectar un router a la VLAN que tenga una ruta para llegar a otra vlan.
- Usar un switch gestionable L3 que permita enrutamiento de tráfico y definir una IP en el interfaz VLAN. 

## Otros protocolos relacionados

- STP (Spanning Three Protocol): protocolo de nivel 2 que permite detectar bucles en la red y desactivar puertos.

## Referencias

- Acceso a estándares de red disponibles de forma gratuita.
  https://ieeexplore.ieee.org/browse/standards/get-program/page/series?id=68

