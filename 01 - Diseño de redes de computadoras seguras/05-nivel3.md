# El nivel 3
La capa de red es una capa compleja que proporciona conectividad y selección de ruta entre dos sistemas de hosts que **pueden estar ubicados en redes geográficamente distintas**. Si desea recordar la Capa 3 en la menor cantidad de palabras posible, piense en selección de **ruta** y direccionamiento. 

En resumen, este nivel nos permite direccionar equipos que se encuentran fuera de nuestra red, de modo que podemos enviarles tráfico a través de una ruta de equipos.

## Configuración de red
En este nivel van a ser importantes los siguientes parámetros:

- Dirección IP
- La máscara de red
- La puerta de enlace

### Dirección IP
La dirección IP es una etiqueta numérica, por ejemplo "192.0.10.1" que identifica, de manera lógica a una interfaz en una red IP.

Una dirección IP tiene dos funciones principales: identificación de la interfaz de red y direccionamiento para su ubicación. 

![Dirección IP](recursos/direccion-ip.png)

### La máscara de red
La máscara de red es una combinación de bits que sirve en el ámbito de las redes de ordenadores, y​ cuya función es indicar a los dispositivos qué parte de la dirección IP es el número de la red, incluyendo la subred, y qué parte es la correspondiente al host. 

![Máscara de red](recursos/mascara-de-red.png)

### La puerta de enlace
El gateway o «puerta de enlace» es normalmente un equipo informático configurado para dotar a las máquinas de una red local (LAN) conectadas a él de un acceso hacia otras redes. 

Podemos decir que una puerta de enlace o un router va a ser un equipo que dispone de conexión directa a varias redes de modo que permite que los equipos conectados a ambas redes puedan comunicarse con el resto.

![Puerta de enlace](recursos/puerta-de-enlace.jpg)

## Asignación de direcciones IP
A la hora de asignar direcciones IP a nuestros equipos disponemos de dos opciones:
- **Estática**: en la configuración de red de cada uno de los equipos de red.
- **Dinámica**: Un servidor DHCP se encarga de hacer asignaciones de direcciones IP a los equipos que se conectan a la red.

## Rangos de direcciones reservados para uso privado
Estos rangos de direcciones IP son los que se pueden utilizar en redes privadas: 

- **Clase A**: 10.0.0.0 a 10.255.255.255
- **Clase B**: 172.16.0.0 a 172.31.255.255
- **Clase C**: 192.168.0.0 a 192.168.255.255
- **Clase B**: 169.254.0.0 – 169.254.255.255 Esta red es para equipos en que no responde el servidor DHCP. Toman una UP aleatoria del rango.
## Herramientas
En este apartado muestro algunas herramientas esenciales que pueden ser utilizadas para gestionar algunas de las opciones del nivel 3.

### Windows

Para mostrar las direcciones IP
```
netsh interface ipv4 show addresses

Configuración para la interfaz "Conexión de red Bluetooth 2"
    DHCP habilitado:                         Sí
    Métrica de interfaz:                      65

Configuración para la interfaz "Loopback Pseudo-Interface 1"
    DHCP habilitado:                         No
    Dirección IP:                           127.0.0.1
    Prefijo de subred:                        127.0.0.0/8 (máscara 255.0.0.0)
    Métrica de interfaz:                      75
```

```
Get-NetIPAddress

IPAddress         : 127.0.0.1
InterfaceIndex    : 1
InterfaceAlias    : Loopback Pseudo-Interface 1
AddressFamily     : IPv4
Type              : Unicast
PrefixLength      : 8
PrefixOrigin      : WellKnown
SuffixOrigin      : WellKnown
AddressState      : Preferred
ValidLifetime     :
PreferredLifetime :
SkipAsSource      : False
PolicyStore       : ActiveStore

```
### Linux
Para mostrar las direcciones IP:

```shell
$ ifconfig
eth0: flags=4163<UP,BROADCAST,RUNNING,MULTICAST>  mtu 1500
        inet 192.168.40.128  netmask 255.255.255.0  broadcast 192.168.40.255
        inet6 fe80::2060:3a33:4201:7621  prefixlen 64  scopeid 0x20<link>
        ether 00:0c:29:7c:77:0d  txqueuelen 1000  (Ethernet)
        RX packets 7  bytes 988 (988.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 23  bytes 3334 (3.2 KiB)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0

lo: flags=73<UP,LOOPBACK,RUNNING>  mtu 65536
        inet 127.0.0.1  netmask 255.0.0.0
        inet6 ::1  prefixlen 128  scopeid 0x10<host>
        loop  txqueuelen 1000  (Local Loopback)
        RX packets 4  bytes 240 (240.0 B)
        RX errors 0  dropped 0  overruns 0  frame 0
        TX packets 4  bytes 240 (240.0 B)
        TX errors 0  dropped 0 overruns 0  carrier 0  collisions 0


```

```bash
$ ip addr show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:0c:29:7c:77:0d brd ff:ff:ff:ff:ff:ff
    inet 192.168.40.128/24 brd 192.168.40.255 scope global dynamic noprefixroute eth0
       valid_lft 1700sec preferred_lft 1700sec
    inet6 fe80::2060:3a33:4201:7621/64 scope link noprefixroute 
       valid_lft forever preferred_lft forever
```

### En ambos sistemas operativos
Para gestionar las rutas 

- route
- netstat 

Para hacer pruebas

- ping: permite enviar un paquete de prueba a un equipo.
- tracert: comprobar la ruta seguida por un paquete para alcanzar el destino.






