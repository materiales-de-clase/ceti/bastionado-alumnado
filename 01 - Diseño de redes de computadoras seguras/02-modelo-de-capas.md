# Modelo de capas en una red
El modelo de capas de una red surgió como respuesta al problema de que cada fabricante implementaba su propia solución de red y muchas veces era incompatible con el hardware de otros fabricantes.

El modelo de capas es una abstracción de una red en la que segmentamos o dividimos una conexión en capas independientes una de otra para descomponer el sistema en partes más pequeñas, más fáciles de analizar y de resolver. Cada capa recibe los datos  de la capa superior o inferior, los procesa  y se los devuelve a la siguiente capa.

![Modelo OSI](recursos/modelo_osi.png)

1. **Capa física**: OSI llega más abajo y defina también una capa para los elementos físicos de la conexión. Gestiona los procedimientos a nivel electrónico en el enlace, como por ejemplo el modem.
2. **Capa de enlace**: corresponde a la capa de acceso al medio de TCP/IP
3. **Capa de red**: hace las mismas funciones que la capa de Internet de TCP/IP
4. **Capa de transporte**: esta se corresponde a la capa de transporte del otro modelo
5. **Capa de sesión**: aquí comienzan las capas orientadas a aplicación, siendo esta la que se encarga de controlar y mantener activo el enlace de los hosts y así poder entregar la información
6. **Capa de presentación**: esta asegura que los datos entregados sean de nuevo ensamblados y presentados de forma coherente al usuario
7. **Capa de aplicación**: en esta capa se definen las aplicaciones que permiten al usuario ejecutar acciones y comandos que resultan en una transmisión a través de la red.

## Modelo de red TCP/IP vs OSI

![Modelo TCP/IP VS OSI](recursos/tcpip-vs-osi.jpg)

