# DMZ
Una zona desmilitarizada o DMZ es una red aislada que se encuentra dentro de la red interna de la organización. En ella se encuentran ubicados exclusivamente todos los recursos de la empresa que deben ser accesibles desde Internet, como el servidor web o de correo. Un ejemplo de una red local con una DMZ es:


![DMZ](recursos/dmz-01.png)

Por lo general, una DMZ permite las conexiones procedentes tanto de Internet, como de la red local de la empresa donde están los equipos de los trabajadores, pero las conexiones que van desde la DMZ a la red local, no están permitidas. Esto se debe a que los servidores que son accesibles desde Internet son más susceptibles a sufrir un ataque que pueda comprometer su seguridad. Si un ciberdelincuente comprometiera un servidor de la zona desmilitarizada, tendría muchos más complicado acceder a la red local de la organización, ya que las conexiones procedentes de la DMZ se encuentran bloqueadas.

## Configuración
Para configurar una zona desmilitarizada en la red de la organización, es necesario contar con un cortafuegos o firewall. Este dispositivo, será el encargado de segmentar la red y permitir o denegar las conexiones. En la siguiente tabla, de manera somera, se muestra el tipo de conexiones recomendables que permitiría o denegaría el firewall dependiendo su origen y destino:

![DMZ](recursos/dmz-politica.png)

![DMZ](recursos/dmz-politica-2.png)

## Configuraciones de doble firewall
Si se quiere aumentar aún más la seguridad de la red interna frente a un ataque proveniente de la DMZ, se pueden ubicar **dos firewall**.

![DMZ](recursos/dmz-dos-firewall.png)

## Ejemplos de configuraciones

#### Configuracion 1
![DMZ](recursos/dmz-configuracion1.png)

#### Configuracion 2
![DMZ](recursos/dmz-configuracion2.png)

#### Configuracion 3
![DMZ](recursos/dmz-configuracion3.png)