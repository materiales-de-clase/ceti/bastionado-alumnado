# Tarea 01 - Analizadores de protocolos
En el siguiente texto tenemos una práctica de la Universidad Politécnica de Navarra relacionada con los analizadores de protocolos. Vamos a hacerls para familiarizarnos un poco con estas herramientas. Dispones del enunciado completo en el siguiente enlace:

- [Práctica 3](recursos/practica-wireshark-tcpdump.pdf)

## Enunciado
Lee atentamente el texto y resuelve los ejercicios en el punto 5 y el punto 6. 

## Antes de empezar
Lee las características de la entrega antes de empezar de modo que puedas prepararla a medida que haces los ejercicios.

En los ejercicios de tcpdump no podrás utilizar el servidor FTP que te indican. Será necesario por contra que utilices los siguientes datos:

- servidor: ftp.rediris.es
- usuario: anonymous
- No hay fichero welcome.txt. En su lugar es otro con un nombre parecido. Averigua el nombre y descárgalo como te indican en el ejercicio.

## Entrega

- Documento pdf apellidos_nombre_tarea0301.pdf.
- Debe contener dos apartados. Uno para wireshark y otro para tcpdump. No olvides escribir la pregunta siempre antes de la respuesta.
- Para las preguntas de wireshark, describe el proceso de configuración de la captura.
- Para cada pregunta, junto a la respuesta, pega una captura de wireshark donde marques el sitio de donde has cogido el dato.
- En los ejercicios de tcpdump, más que el fichero, lo que interesa es el contenido. Pega el contenido (claramente delimitado) dentro de la respuesta a cada pregunta. Usa texto más que imágenes en este caso. En este caso es importante que muestres el comando que has lanzado antes de la salida.
- En estos ejercicios podrías necesitar utilizar wireshark o un editor para analizar el fichero generado. En ese caso pega captura donde marques la información que has utilizado para obtener la respuesta.

## Ejemplos

### Grafica
Ejemplo de configuración para que se vean los paquetes de ida y vuelta en una gráfica que representa varios ping a un servidor. Con esa configuración, al hacer zoom sobre la gráfica se ven los paqueres de ida y de vuelta.

![Grafica](recursos/03t-adp-tarea01-grafica.png)