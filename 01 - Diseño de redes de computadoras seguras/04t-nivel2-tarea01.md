# Averigua el fabricante
Esta práctica consiste en averiguar el fabricante de un portátil cualquiera de la red utilizando para ello la dirección MAC.

Ya hemos hablando anteriormente de qué es una mac y cómo puedes obtener la MAC de tu equipo o de otros equipos con los que el tuyo se comunica. Utilizando esta web:

http://www.coffer.com/mac_find/ 

Deberás buscar el fabricante de la tarjeta de red de tu PC a partir de la MAC. Como respuesta a la tarea, adjunta un PDF con apellidos_nombre_tarea_0401.pdf. En el PDF incluye captura del proceso por el que has obtenido la MAC del PC y  y los resultados de la web. Escribe además en una pequeña conclusión los resultados.

Importante. Cuida la estructura del documento. Crea apartados y acompaña a las imágenes de texto que ayude a entender lo que estás poniendo.

