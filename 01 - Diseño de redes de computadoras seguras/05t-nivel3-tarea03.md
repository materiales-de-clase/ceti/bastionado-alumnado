# ¿Cómo funciona el enrutamiento?
En este ejercicio vamos a analizar cómo se lleva a cabo el enrutamiento de paquetes en una red IP utilizando Wireshark. Para ello:

- Limpia la tabla arp del equipo que vas a utilizar para las pruebas.
- Configura Wireshark para que capture tráfico únicamente de cliente y servidor.
- Haz ahora ping al servidor escogido. Vale cualquier servidor de internet.
- Analiza el proceso y describe cómo hace tu PC para enviar tráfico a internet a través del router.

# Entrega
- Entrega en el archivo apellidos_nombre_nivel3_tarea03.pdf. El archivo debe contener todos los integrantes del equipo.
- Describe todo el proceso de preparación y captura seguido utilizando texto y capturas.
- Describe que ha pasado.
- Si incluyes capturas para iluestrar el proceso marca las partes que has utilizado para obtener conclusiones.


