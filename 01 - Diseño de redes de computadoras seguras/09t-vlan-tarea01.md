# Tarea 01 
En este ejercicio vamos a tratar de echar un vistazo a las etiquetas de las VLAN. 

## Pasos previos
Para poder realizar la tarea vamos a necesitar lo siguiente:

- 2 Portátiles con el software Realtek Ethernet Diagnostic Utility instalado.
- 1 Portátil con wireshark para capturar trático
- 1 switch gestionable con soporte para VLAN 802.1Q y port mirror.

Vamos a tener que configurar los diferentes elementos del siguiente modo:

#### Conexiones de red
El switch quedará del siguiente modo:

1 -> Portátil
2 -> trunk <- Portátil con la herarmienta de realtek
3
4 -> Trunk
5 -> Trunk
6 -> Port mirror <- Se conecta el portátil para captura 
7 -> trunk <- Portátil con la herarmienta de realtek
8 -> Portátil

#### Configuración de la VLAN.
En el switch vamos a definir las VLANs 999 y 888 y las vamos a pasar etiquetadas por los puertos 2 y 7. 

En los portátiles vamos a utilizar unos adaptadores USB de realtek. En dichos adaptadores, utilizando la herramienta Realtek Diagnostic Utility vamos a configurar configurar la VLAN 999 y 888 en cada uno de los portátiles. Esto permitirá conectar el portátil a dicha VLAN. Asignaremos a ambos portátiles las siguientes direcciones IP:
    - VLAN 888: 172.16.0.1/24 y 172.16.0.2/24
    - VLAN 999: 172.16.1.1/24 y 172.16.1.2/24

Si hemos hecho todo correctamente, deberíamos poder hacer ping entre ambos portátiles por ambas redes.

#### Configuración del equipo con Wireshark
Ahora configuraremos el PortMirror para que envíe el tráfico que sale por los puertos de red 2 y 7 al puerto 6. Conectaremos ahí el portátil y arrancaremos wireshark para capturar tráfico de la red 172.16.0.0/16. Deberíamos poder ver el tráfico entre ambos equipos.

## Enunciado
Para este ejercicio vamos a tratar de capturar las tramas ethernet en formato 802.1Q. Una vez que tengas los pasos previos realizados y hayas comprobado que funciona, haz una captura de un paquete de tipo ping para las dos redes que hemos configurado previamente. 

- En el documento, pega la información que te de wireshark sobre la etiqueta (donde aparece y los valores) para los paquetes de las dos vlan.
- Pega también una trama sin etiquetar.
- Compara las tres tramas a nivel de campos que aparecen y sus valores. Presta especial atención a los datos que sabemos que 802.1Q modifica: 
  - El campo Ethertype vale 0x8100
  - Se añaden dos bytes a modo de etiqueta para codificar:
    - 3 bits: Prioridad
    - 1 bit: Canonical Format Indicator (CFI) usado para indicar la presencia de un campo Routing Information Field (RIF)
    - 12 bits: VLAN ID o VID que identifica de forma única la VLAN a la que 

Para que sea más sencillo de ver, crea una tabla con las columnas:

- IP Origen 
- IP Destino
- Ethertype
- Prioridad
- CFI
- VID

## Entrega
Entrega un PDF con la tabla que se indica y las capturas realizadas durante el proceso. 

