# ¿Cómo funciona el protocolo ARP?
En esta tarea vas a analizar el funcionamiento del protocolo ARP utilizando wireshark. Sigue los siguientes pasos para realizar la práctica:

- Borra tabla arp de tu equipo.
- Haz ping a una ip que exista en la red verifica que ha pasado comprueba la  tabla arp. Verifica si hay cambios.
- Haz ping ahora una ip dentro del rango válido de la red y que no esté asignada a ningún equipo. Verifica que ha pasado. Comprueba la tabla ARP.
- Describe ahora con tus palabras cómo funciona el protocolo ARP. Intenta estructurarlo de forma sencilla como un esquema que sea fácil de entender a simple vista.
- ¿Para qué sirve el protocolo ARP?
- ¿Piensas que puede haber algún punto débil en este protocolo? Reflexiona y contesta a la pregunta.

# Entrega
En esta tarea debes entregar un PDF apellidos_nombre_nivel2_tarea02_arp.pdf. Debes incluir:

- Los pasos realizados junto con capturas y explicaciones que dejen seguir de forma clara la realización del ejercicio.
- En las trazas con el analizador de protocolos, capturas que marquen las partes de donde saque las información o conclusiones.
- Descripción lo más precisa posible de como funciona el protocolo ARP.

