# El nivel 2. La capa de enlace de datos
La capa de enlace de datos es la segunda capa del modelo OSI. Su función principal es la transferencia fiable dentro del circuito de transmisión de datos. Como capa intermedia, recibe peticiones de su capa superior, la capa de red, y utiliza los servicios ofrecidos por su capa inferior, la física.

En este nivel nos vamos a encontrar con dispositivos de enlace que nos permiten conectar equipos utilizando los servicios de la capa 1. Podemos tener por ejemplo:

- **Nuestros interfaces físicos** de red
- **Interfaces de red lógicos**. Son interfaces creados por software que generalmente tendrán que estar asociados a una interfaz física.

Las comunicaciones a nivel 2 ocurren siempre dentro de una red LAN. Dispositivos que utilizamos en una red local para permitir comunicaciones de nivel 2 que todos conocemos son:

- switches
- hubs

## Conceptos
Hay algunos conceptos que podemos tener en cuenta cuando hablamos del nivel 2.

### Dirección MAC
Como hemos comentado en el apartado anterior, la dirección MAC es un identificador hexadecimal único y viene representado por seis grupos de dos cifras llamados octetos. Por lo que resulta imposible encontrar dos dispositivos de red con la misma dirección MAC.

![Dirección MAC](recursos/direccion_mac.png)

A la vista de esta definición de la dirección. Calcula los siguientes datos:

- Número máximo de direcciones MAC diferentes que podemos tener.
- Número máximo de direcciones MAC que puede tener asociadas un fabricante de tarjetas.

### Dominio de colisión
Los dominios de colisión son los puntos de la red en que los mensajes pueden chocar. Dos equipos tratan de enviar en el medio al mismo tiempo.

![Dominio de colisión](recursos/dominio_colision.jpg)

### Dominio de broadcast
Un dominio de broadcast —que podríamos traducir como dominio de difusión— es una separación lógica dentro de la red de ordenadores en la que los mensajes, pueden ser difundidos para que **todos los equipos** dentro de ese espacio los puedan recibir.

![Dominio de broadcast](recursos/dominio_broadcast.jpg)


## Herramientas
Vamos a probar ahora algunas de las herramientas que tenemos disponibles en este nivel de la red.

### Obtener información de los adaptadores
#### ipconfig (windows)
Herramienta de windows que permite obtener la configuración IP de un interfaz. Además, podemos utilizarlo para obtener la dirección MAC del adaptador de red de nuestro PC.

Ejemplo de salida del comando *ipconfig /all*
```
Adaptador de Ethernet Ethernet:

   Sufijo DNS específico para la conexión. . :
   Descripción . . . . . . . . . . . . . . . : Intel(R) Ethernet Controller I225-V
   Dirección física. . . . . . . . . . . . . : D8-5E-D3-29-B0-9A
   DHCP habilitado . . . . . . . . . . . . . : sí
   Configuración automática habilitada . . . : sí
   Vínculo: dirección IPv6 local. . . : fe80::3c50:5794:22a2:d51e%8(Preferido)
   Dirección IPv4. . . . . . . . . . . . . . : 192.168.1.166(Preferido)
   Máscara de subred . . . . . . . . . . . . : 255.255.255.0
   Concesión obtenida. . . . . . . . . . . . : jueves, 29 de septiembre de 2022 9:48:06
   La concesión expira . . . . . . . . . . . : viernes, 30 de septiembre de 2022 9:48:06
   Puerta de enlace predeterminada . . . . . : 192.168.1.1
   Servidor DHCP . . . . . . . . . . . . . . : 192.168.1.1
   IAID DHCPv6 . . . . . . . . . . . . . . . : 618159827
   DUID de cliente DHCPv6. . . . . . . . . . : 00-01-00-01-2A-5C-D5-D0-00-50-B6-13-72-FC
   Servidores DNS. . . . . . . . . . . . . . : 192.168.1.1
   NetBIOS sobre TCP/IP. . . . . . . . . . . : habilitado
```

#### netsh (windows)
Utilidad en línea de comandos que permite la configuración de una red.

Ejemplo para obtener la dirección mac

```
  netsh lan show interfaces
```

#### getmac (windows)
Permite obtener las direcciones MAC de los adaptadores.

```
Dirección física    Nombre de transporte
=================== ==========================================================
0A-00-27-00-00-12   \Device\Tcpip_{989261F6-2DAD-45EE-A3EE-5EE76A21AF4D}
00-50-B6-13-72-FC   Medios desconectados
00-50-56-C0-00-01   \Device\Tcpip_{96916736-6F02-4440-B9C2-54CCEEF89457}
00-50-56-C0-00-08   \Device\Tcpip_{F4E22825-D1A5-4C5F-B0C0-1EC3B628F727}
D8-5E-D3-29-B0-9A   \Device\Tcpip_{3A1CA65D-C3B6-4DF4-BC59-EEFD747B55D6}
B0-A4-60-CF-90-45   Medios desconectados
B0-A4-60-CF-90-49   Medios desconectados
```

#### ifconfig
Comando en linux que podemos utilizar para listar y configurar los interfaces de red. Podemos emplear este comando también para obtener la dirección mac del adaptados.

Salida del comando ifconfig
```
eth0      Link encap:Ethernet  HWaddr 09:00:12:90:e3:e5  
          inet addr:192.168.1.29 Bcast:192.168.1.255  Mask:255.255.255.0
          inet6 addr: fe80::a00:27ff:fe70:e3f5/64 Scope:Link
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:54071 errors:1 dropped:0 overruns:0 frame:0
          TX packets:48515 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:22009423 (20.9 MiB)  TX bytes:25690847 (24.5 MiB)
          Interrupt:10 Base address:0xd020 
```
#### ip (linux)
Podemos utilizar por ejemplo el comando *ip link show*

```
1: lo:  mtu 16436 qdisc noqueue state UNKNOWN 
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eth0:  mtu 1500 qdisc mq state UP qlen 1000
    link/ether b8:ac:6f:65:31:e5 brd ff:ff:ff:ff:ff:ff
```

#### arp (windows/lixux)
Este comando está disponible en linux y windows, aunque las opciones en ambos sistemas pueden diferir.

Muestra y permite modificar las tablas del protocolo ARP, encargado de convertir las direcciones IP de cada ordenador en direcciones MAC (dirección física única de cada tarjeta de red).

- arp -v: Muestra las entradas actuales de la tabla arp.
- arp -d: Elimina un host de la tabla ARP 
- arp -s: Agrega un host a la tabla ARP


### Pruebas de red
#### arping (linux)
arping es una utilidad Linux que permite el envío de paquetes ARP a otros equipos conectados a la red.

  arping FF:AD:FF:...

