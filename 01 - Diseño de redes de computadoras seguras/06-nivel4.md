# El nivel 4
La capa de transporte TCP/IP **garantiza que los paquetes lleguen en secuencia y sin errores**, al intercambiar la confirmación de la recepción de los datos y retransmitir los paquetes perdidos. Este tipo de comunicación se conoce como transmisión de punto a punto. Los protocolos de capa de transporte de este nivel son:

- el Protocolo de control de transmisión (TCP) 
- el Protocolo de datagramas de usuario (UDP)
- el Protocolo de transmisión para el control de flujo (SCTP). 

Los protocolos TCP y SCTP proporcionan un servicio completo y fiable. UDP proporciona un servicio de datagrama poco fiable. 

Si en la capa anterior se añadían las direcciones IP, en esta capa, se utilizan puertos para identificar los extremos de las conexiones entre las aplicaciones. Más adelante veremos alguna información sobre los puertos.

## El protocolo TCP
TCP permite a las aplicaciones comunicarse entre sí como si estuvieran conectadas físicamente. TCP envía los datos en un formato que se **transmite carácter por carácter** (flujo de datos), en lugar de transmitirse por paquetes discretos. Esta transmisión consiste en lo siguiente:

- Punto de partida, que abre la conexión.
- Transmisión completa en orden de bytes.
- Punto de fin, que cierra la conexión.

TCP conecta un encabezado a los datos transmitidos. Este encabezado contiene múltiples parámetros que ayudan a los procesos del sistema transmisor a conectarse a sus procesos correspondientes en el sistema receptor.

TCP confirma que un paquete ha alcanzado su destino estableciendo una conexión de punto a punto entre los hosts de envío y recepción. Por tanto, el protocolo TCP se considera un protocolo fiable orientado a la conexión.

## Protocolo SCTP
El SCTP (stream control transmission protocol) es un protocolo fiable de la familia de protocolos de Internet que permite la **transmisión de mensajes de telecomunicación a través de redes IP**. Reúne diversas características de dos protocolos también encargados de la transferencia de datos: el TCP (orientado a la conexión) y el UDP (sin conexión). Además, contiene, entre otras cosas, mecanismos para controlar la congestión y mejorar la tolerancia de fallos en el envío de paquetes. Gracias a su gran flexibilidad, el SCTP también se usa para otros fines (por ejemplo, para controlar y administrar grupos de servidores).

El SCTP usa normalmente la dirección IP como base, pero en principio puede usarse sobre cualquier otro servicio de mensajería sin conexión. El transporte de paquetes se caracteriza en este caso por los siguientes aspectos:

- Transmisión con confirmación de datos de usuario (sin fallos ni duplicados)
- Fragmentación de datos para poder ajustarse al tamaño máximo de paquete de la ruta de red
- Entrega secuenciada de mensajes de usuario dentro de múltiples corrientes de datos (multistreaming), incluyendo la opción de determinar el orden de dichos mensajes
- Agrupación (opcional) de varios mensajes de usuario en un solo paquete SCTP (chunk bundling)
- Tolerancia de fallos a nivel de red gracias al multihoming (host con varias direcciones de red válidas) de uno o de ambos participantes en la comunicación

## Protocolo UDP
UDP proporciona un servicio de entrega de datagramas. UDP no verifica las conexiones entre los hosts transmisores y receptores. Dado que el protocolo UDP elimina los procesos de establecimiento y verificación de las conexiones, resulta ideal para las aplicaciones que envían pequeñas cantidades de datos. 

Algunos usos de UDP:

- Consultas DNS
- Conexiones VPN
- Streaming de audio y vídeo

Generalmente se va a usar en aplicaciones donde es tolerable que exista pérdida o donde la pérdida, si se da, la gestiona la propia aplicación.

## Los puertos
Los puertos se identifican con un número de 16 bits, es decir, están en el rango 0-65535. En el [rfc6335](https://www.rfc-editor.org/rfc/rfc6335) se definen los procedimientos seguidos por la IANA (Internet Assigned Numbers Authority) en lo que respecta a la asignación y gestión de otros asuntos relacionados con el registro de puertos.

En este RFC nos encontramos con que se definen los siguientes rangos de puertos:

- 0 - 1023: System Ports, also known as the Well Known Ports. Asignados por la IANA
- 1024 - 49151: User Ports, also known as the Registered Ports. Asignados por la IANA
- 49152 – 65535: Dynamic Ports, also known as the Private or Ephemeral Ports. Estos puertos no se asignan.

En dicho RFC se especifican los requisitos para que se pueda registrar un puerto en el primer o segundo bloque de puertos. Generalmente se creará una entrada en el segundo bloque salvo que se cumplan los requisitos para que se registre en el primero. 

En la siguiente URL podemos acceder al registro de puertos. En dicho registro podemos localizar las entradas que se encuentran registradas en estos momentos:

https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml

Un puerto del grupo 0-49151 puede estar en uno de los siguientes estados:

- Asignado: Está asignado
- No asignado: Está disponible para ser solicitado
- Reservado: Están asignados a la IANA para propósitos especiales


## Herramientas relacionadas con los servicios en el nivel 4
En este apartado vamos a probar algunas herramientas que podemos utilizar para hacer pruebas relacionadas con el nivel 4, es decir, que nos permitan hacer pruebas con puertos.

### netstat (windows/linux)
Herramienta que permite obtener estadísticas de red. Con el siguiente comando puedes listar todos los puertos que están en uso en un equipo.

```bash
    netstat -na
```

### Get-NetTCPConnection (powershell)
Comando que permite obtener información sobre los puertos en una máquina.

```ps
PS C:\Users\Juanjo>  Get-NetTCPConnection

LocalAddress                        LocalPort RemoteAddress                       RemotePort State       AppliedSetting
------------                        --------- -------------                       ---------- -----       --------------
::                                  60967     ::                                  0          Bound
::1                                 60967     ::1                                 60966      Established Internet
::1                                 60966     ::1                                 60967      Established Internet
::                                  49673     ::                                  0          Listen
::                                  49669     ::                                  0          Listen
::                                  49668     ::                                  0          Listen
::                                  49667     ::                                  0          Listen
::                                  49666     ::                                  0          Listen
::                                  49665     ::                                  0          Listen
::                                  49664     ::                                  0          Listen
::                                  7680      ::                                  0          Listen
::                                  3389      ::                                  0          Listen
::                                  445       ::                                  0          Listen
::                                  135       ::                                  0          Listen
0.0.0.0                             64414     0.0.0.0                             0          Bound
0.0.0.0                             64411     0.0.0.0                             0          Bound
0.0.0.0                             64410     0.0.0.0                             0          Bound
0.0.0.0                             64409     0.0.0.0                             0          Bound
0.0.0.0                             64408     0.0.0.0                             0          Bound
0.0.0.0                             64406     0.0.0.0                             0          Bound
0.0.0.0                             64405     0.0.0.0                             0          Bound
0.0.0.0                             60973     0.0.0.0                             0          Bound
0.0.0.0                             60965     0.0.0.0                             0          Bound
0.0.0.0                             60934     0.0.0.0                             0          Bound
```

### tcping
Herramienta que podéis descargar desde 
https://www.elifulkerson.com/projects/tcping.php 

Permite hacer ping a un puerto concreto. A continuación una prueba al puerto del FTP
```batch
C:\>tcping64.exe ftp.rediris.es 21

Probing 130.206.13.2:21/tcp - Port is open - time=9.995ms
Probing 130.206.13.2:21/tcp - Port is open - time=10.542ms
Control-C

Ping statistics for 130.206.13.2:21
     2 probes sent.
     2 successful, 0 failed.  (0.00% fail)
Approximate trip times in milli-seconds:
     Minimum = 9.995ms, Maximum = 10.542ms, Average = 10.268ms
```

Aquí tenemos una prueba al puerto 22, que aparece como cerrado. No hay respuesta.
```batch
C:\>tcping64.exe ftp.rediris.es 22

Probing 130.206.13.2:22/tcp - No response - time=2009.640ms
Probing 130.206.13.2:22/tcp - No response - time=2002.808ms
Control-C
Probing 130.206.13.2:22/tcp - No response - time=376.931ms

Ping statistics for 130.206.13.2:22
     3 probes sent.
     0 successful, 3 failed.  (100.00% fail)
Was unable to connect, cannot provide trip statistics.
```
### nc (netcat) (linux/windows)
Esta herramienta permite intercambiar datos entre un cliente y un servidor desde nuestros scripts. Disponéis de más información aquí:

- [Página del proyecto](https://nmap.org/ncat/)
- [Manual de Usuario](https://nmap.org/ncat/guide/index.html)

Por ejemplo, con la siguiente secuencia podemos obtener la versión de un servidor SSH
```batch
$ echo "EXIT" | nc localhost 22
SSH-2.0-OpenSSH_9.0p1 Debian-1+b1
Invalid SSH identification string.                           
```

### nmap (linux/windows)
NMap es una herramienta que podemos utilizar para obtener información de la red. La página oficial es:

https://nmap.org/download.html

Esta herramienta puede ser utilizada para muchos fines. Vamos a ver algunos ejemplos.

Comprobar si un puerto concreto está abierto
```batch
C:\> nmap -sU -p 21,22 ftp.rediris.es
Starting Nmap 7.93 ( https://nmap.org ) at 2022-10-19 16:53 Hora de verano romance
Nmap scan report for ftp.rediris.es (130.206.13.2)
Host is up (0.0090s latency).

PORT   STATE         SERVICE
21/udp open|filtered ftp
22/udp open|filtered ssh

Nmap done: 1 IP address (1 host up) scanned in 1.65 seconds
```

Comprobar si un puerto está abierto en todas las máquinas de la red.
```batch
C:\> nmap -sU -p 21,22 192.168.90.1-254
Ejecutar
```

Con --help podemos obtener información acerca de las posibles configuraciones que podemos hacer del escaneo.

### nping
Otra herramienta que podemos utilizar para analizar la red.

A continuación tenemos una prueba utilizando esta herramienta para validar si los puertos 21 o 22 están abiertos.
```bash
└─$ sudo nping --tcp -p 21 ftp.rediris.es

Starting Nping 0.7.92 ( https://nmap.org/nping ) at 2022-10-19 11:28 EDT
SENT (0.0617s) TCP 192.168.40.128:41964 > 130.206.13.2:21 S ttl=64 id=2732 iplen=40  seq=3609306717 win=1480 
RCVD (0.0799s) TCP 130.206.13.2:21 > 192.168.40.128:41964 SA ttl=128 id=65250 iplen=44  seq=1042225353 win=64240 <mss 1460>
SENT (1.0620s) TCP 192.168.40.128:41964 > 130.206.13.2:21 S ttl=64 id=2732 iplen=40  seq=3609306717 win=1480 
RCVD (1.0722s) TCP 130.206.13.2:21 > 192.168.40.128:41964 SA ttl=128 id=65251 iplen=44  seq=962344864 win=64240 <mss 1460>
SENT (2.0635s) TCP 192.168.40.128:41964 > 130.206.13.2:21 S ttl=64 id=2732 iplen=40  seq=3609306717 win=1480 
RCVD (2.0744s) TCP 130.206.13.2:21 > 192.168.40.128:41964 SA ttl=128 id=65252 iplen=44  seq=130823573 win=64240 <mss 1460>
^C 
Max rtt: 18.057ms | Min rtt: 10.125ms | Avg rtt: 13.003ms
Raw packets sent: 3 (120B) | Rcvd: 3 (138B) | Lost: 0 (0.00%)
Nping done: 1 IP address pinged in 2.66 seconds                                                                                                                                                                                                                                         
┌──(kali㉿kali)-[~]
└─$ sudo nping --tcp -p 22 ftp.rediris.es

Starting Nping 0.7.92 ( https://nmap.org/nping ) at 2022-10-19 11:28 EDT
SENT (0.0616s) TCP 192.168.40.128:34748 > 130.206.13.2:22 S ttl=64 id=22642 iplen=40  seq=3094786073 win=1480 
SENT (1.0620s) TCP 192.168.40.128:34748 > 130.206.13.2:22 S ttl=64 id=22642 iplen=40  seq=3094786073 win=1480 
SENT (2.0638s) TCP 192.168.40.128:34748 > 130.206.13.2:22 S ttl=64 id=22642 iplen=40  seq=3094786073 win=1480 
SENT (3.0655s) TCP 192.168.40.128:34748 > 130.206.13.2:22 S ttl=64 id=22642 iplen=40  seq=3094786073 win=1480 
SENT (4.0675s) TCP 192.168.40.128:34748 > 130.206.13.2:22 S ttl=64 id=22642 iplen=40  seq=3094786073 win=1480 
^C 
Max rtt: N/A | Min rtt: N/A | Avg rtt: N/A
Raw packets sent: 5 (200B) | Rcvd: 0 (0B) | Lost: 5 (100.00%)
Nping done: 1 IP address pinged in 4.32 seconds
```

### Otras herramientas

- [WinPing](https://www.majorgeeks.com/files/details/winping.html)
- [Putty](https://www.putty.org/)
- [NetworkPinger](http://www.networkpinger.com/en/downloads/)
- También podemos utilizar la herramienta telnet. Sería necesario añadirla a windows o instalarla en Linux