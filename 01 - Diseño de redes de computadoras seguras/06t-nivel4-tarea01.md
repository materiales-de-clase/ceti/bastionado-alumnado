# ¿Cómo funciona el protocolo ftp?
En esta tarea vamos a analizar cómo funcionaría el protocolo de una aplicación desde el punto de vista de las comunicaciones. Para ello vamos a utilizar nuevamente nuestro analizador de protocolos y vamos a analizar el tráfico intercambiado con un servidor. Para añadir un punto interesante, vamos a utilizar la funcionalidad port mirror para monitorizar la sesión entre un cliente y el servidor. 

## Proceso de captura
La captura la vamos a hacer con tcpdump y desde otro portátil. Tendrás que hacer lo siguiente:

- **switch de mesa**
  - Configura la funcionalidad portmirror para enviar el tráfico del switch a un puerto.
- **En portátil de captura**
  - Conecta al puerto que vas a utilizar para hacer la captura.
  - Conecta una Kali Linux al puerto ethernet en modo bridge
  - Ejecuta tcpdump en el interfaz de red y haz una captura de el tráfico entre cliente y servidor únicamente aplicando un filtro. 
  - Exporta toda la captura a un archivo pcap.
  - Descarga desde tu PC este archivo
- **En el PC de captura**
  - Importa ahora la captura con Wireshark para hacer el análisis
- **En portátil cliente**
  - Sigue los pasos indicados en la sesión de FTP.

## Sesión de FTP
Puedes utilizar cualquier cliente de FTP, pero si quieres utilizar la línea de comandos puedes utilizar los siguientes comandos:

```ftp
$ ftp ftp.rediris.es
Trying 130.206.13.2:21 ...
Connected to ftp.rediris.es.
220-  Bienvenido al servicio de replicas de RedIRIS.
220-     Welcome to the RedIRIS mirror service.
220 Only anonymous FTP is allowed here
Name (ftp.rediris.es:kali): anonymous
230-            RedIRIS - Red Académica y de Investigación Española
230-                RedIRIS - Spanish National Research Network
230-
230-           ftp://ftp.rediris.es  -=-  http://ftp.rediris.es
230 Anonymous user logged in
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> ls
229 Extended Passive Mode Entered (|||48668|)
150 Accepted data connection
drwxr-xr-x    4 14         50               4096 Sep 20  2017 .
drwxr-xr-x    4 14         50               4096 Sep 20  2017 ..
lrwxrwxrwx    1 14         50                 23 Jun  8  2017 debian -> sites/debian.org/debian
lrwxrwxrwx    1 14         50                 26 Jul 18  2017 debian-cd -> sites/debian.org/debian-cd
drwxr-xr-x    2 14         50               8192 Jun 26  2021 mirror
drwxrwxr-x   60 14         50               8192 Jun 25  2021 sites
-rw-r--r--    1 14         50                 93 Jun  8  2017 welcome.msg
226-Options: -a -l 
226 7 matches total
ftp> get welcome.msg
local: welcome.msg remote: welcome.msg
229 Extended Passive Mode Entered (|||41770|)
150 Accepted data connection
100% |***********************************************************************************************************************************************************************************************|    93      658.11 KiB/s    00:00 ETA
226-File successfully transferred
226 0.000 seconds (measured here), 0.61 Mbytes per second
93 bytes received in 00:00 (144.38 KiB/s)
ftp> quit
221-Goodbye. You uploaded 0 and downloaded 1 kbytes.
221 Logout.
```

## Preguntas
1. Describe con tus palabras cómo funciona el protocolo ftp de forma general. 
2. Para cada uno de los comandos que has ido lanzando, explica brevemente qué ocurre, es decir, cómo funciona cada comando desde el punto de vista de la red.
3. Pega la parte donde se hace la autenticación del usuario
4. ¿Cuántas conexiones se han establecido y con qué finalidad?

# Entrega
- Entrega las respuestas a cada una de las preguntas acompañadas de capturs de red. Asegúrate de que las capturas son significativas y marcas las cosas importantes de modo que se vea de donde sacas la información.
- Archivo PCAP con la captura de toda la sesión. El archivo solo debe contener el tráfico entre cliente y servidor. No puede contener tramas que no correspondan.
