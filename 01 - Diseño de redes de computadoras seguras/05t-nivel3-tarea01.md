# ¿Cómo funciona el protocolo DHCP?
En esta tarea vas a analizar el funcionamiento del protocolo ARP utilizando wireshark. Para ello.

1. Configura un mirror en el switch de la mesa para redirigir tráfico a un puerto. Si no tienes switch deberás configurar uno.
2. Coge ahora un portátil y conéctalo a ese puerto. Asegura que puedes capturar todo el tráfico que pasa por el switch. Para hacer la captura puedes utilizar el Wireshark
3. Coge ahora otro portátil y conéctalo a un puerto en el switch que esté conectado a la LAN del aula.
4. Necesitarás ahora las direcciones MAC del portátil, el router y broadcast. Debes asegurarte de que capturas todo el tráfico relacionado con estos elementos de red en el portátil que tenías con wireshark configurado.
5. Configura ahora el portátil para que obtenga una dirección IP por DHCP.
6. Utiliza *ipconfig /release* e *ipconfig /renew* para obtener una nueva dirección IP cada vez que necesites repetir el proceso de obtención de una dirección IP. También puedes dehabilitar/habilitar el interfaz de red.
7. Observa el tráfico que se intercambia para obtener la dirección IP y describe paso a paso el protocolo poniendo atención a los tipos de mensajes intercambiados así como origen y destino de todos los mensajes.
8. Intenta también averiguar los parámetros de red que el servidor DHCP envía al equipo que se ha configurado.

# Agrupamientos
Esta práctica va a hacerse en grupos de 3-4 alumnos.

# Entrega
- Documento apellidos_nombre_nivel3_tarea01.pdf. Solo tendrá que entregarla uno de los dos alumnos. Dentro del documento tendrán que aparecer los  alumnos integrantes del grupo.
- PDF donde incluyas la descripción del protocolo acompañada de capturas de pantalla donde MARQUES claramente las partes de la pantalla de donde obtienes la información.
- Adjunta también el fichero de captura en formato pcapng que has utilizado para hacer tu análisis.
- La documentación entregada tiene que tener una estructura adecuada y ser fácil entender.

