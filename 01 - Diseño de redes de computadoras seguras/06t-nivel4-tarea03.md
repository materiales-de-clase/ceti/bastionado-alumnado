# Analiza los puertos abiertos en equipos en la red
En esta práctica vamos a hacer un escaneo en la red para determinar los puertos abiertos en los diferentes equipos. Además, vamos a hacer la prueba con diferentes opciones de configuración en el equipo destino. Para evitar que la prueba dure mucho tiempo, en lugar de buscar en toda la red, lo vamos a hacer un solo equipo.

## Antes de comenzar
- Configura Wiresark en el equipo que va a hacer el scan para analizar luego cómo obtiene nmap toda la información.

## Enunciado 
El objetivo va a ser escanear la red (lo vamos a hacer con un solo equipo) para buscar puertos abiertos utilizando nmap.  Deberemos seguir los siguientes pasos:

1. Anotamos cómo está la configuración de los equipos. Deberás dejarlos como estaban antes de abandonar el aula. 
2. Asegúrate ahora de que el interfaz de red sobre el que se va a hacer el escaneo está configurado en modo red pública de windows. Haz un escaneo de puertos. Guarda los resultados. Deberás pegar los resultados en el informe.
3. Ahora cambiamos la configuración a red privada y hacemos un scan de nuevo. Anotamos los resultados.
4. Finalmente, desactivamos el firewall (detén el servicio) y hacemos otro scan. Anotamos los resultados.

## Entrega
Deberás entregar una tabla que cotenga las siguientes columnas:

- Puerto
- Protocolo (TCP/UDP)
- Modo Público : marca si está accesible
- Modo Privado: marca si está accesible en este modo
- Firewall desactivado: marca si está accesible en este modo.

Describe además con tus palabras en qué consiste el escaneo que hace nmap