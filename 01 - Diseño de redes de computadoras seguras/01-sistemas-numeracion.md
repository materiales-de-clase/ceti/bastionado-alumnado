# Sistemas de numeración
Es importante diferenciar entre la magnitud que estamos representando y su representación. Toda magnitud puede ser representada de diferentes formas. 

Antes de continuar, es conveniente echar un vistazo a las siguientes definiciones:

- **Sistema Numérico**. Se llama sistema numérico al conjunto ordenado de símbolos o dígitos y a las reglas con que se combinan para representar cantidades numéricas. 
- **Dígito**: Un dígito en un sistema numérico es un símbolo que no es combinación de otros y que representa un entero positivo. 
- **Base de un sistema numérico**: La base de un sistema numérico es el número de dígitos diferentes usados en ese sistema. 

En este tema vamos a ver diferentes sistemas numéricos que van a ser de utilidad durante el curso.

## El sistema decimal
El sistema numérico que utilizamos actualmente en todos los países es el Sistema de Numeración Decimal . Está formado por diez símbolos llamados dígitos: 0, 1, 2, 3, 4, 5, 6, 7, 8 y 9 . Con estos dígitos, que se pueden combinar,  se representan todos los números, los cuales sirven para contar y ordenar. 

## Otros sistemas
Aquí tenemos algunos sistemas numéricos empleados en informática y que usaremos durante el curso:

- Binario o base 2. (0, 1)
- Octal o base 8. (0, 1, 2, 3, 4, 5, 6, 7)
- Hexadecimal o base 16. (0, 1, 2, 3, 4, 5, 6, 7, 8, 9, A, B, C, D, E, F)
 
