# Analiza los puertos abiertos en tu equipo
En esta tarea vamos a obtener un informe de las aplicaciones que tienen puertos abiertos en nuestro equipo del mismo modo que podríamos hacerlo en cualquier otro equipos.

# Enunciado
1. Utiliza netstat para ver los puertos abiertos (LISTENING) en tu equipo y el PID del proceso. 
2. Ahora, utilizando el administrador de tareas obtén la siguiente información adicoonal sobre el proceso:
  - usuario propietario
  - la descripción
  - la memoria utilizada
3. Utilizando ahora la siguiente web de la [iana](https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml), comprueba si los puertos están registrados.
4. Utilizando ahora un buscador como [file.net](https://www.file.net/process/), analiza de qué se trata y si es un servicio imprescindible o no.

# Entrega
La entrega va a ser una tabla con las siguientes columnas: 

- Puerto
- Protocolo (TCP/UDP)
- Dirección de escucha
- PID del proceso
- Nombre del proceso
- Propietario
- Descripción
- Descripción con tus palabras de qué es o para qué sirve
- Requerido o no y motivo

