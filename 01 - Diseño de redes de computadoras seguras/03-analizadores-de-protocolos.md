# Analizadores de protocolos
Un analizador de protocolos es una herramienta que sirve para desarrollar y depurar protocolos y aplicaciones de red. Permite:

- Capturar diversas tramas de red para analizarlas, ya sea en tiempo real o después de haberlas capturado. 
- El programa puede reconocer que la trama capturada pertenece a un protocolo concreto (TCP, ICMP...) y muestra al usuario la información decodificada. 
- Se puede utilizar para analizar todo el tráfico de la red y verificar qué está pasando. 

Hay aplicaciones de escritorio y otras que se pueden instalar en un servidor o equipo de red con la finalidad de analizar qué está pasando. Aquí tenemos que distinguir entre dos funciones:

- La función de captura, que puede encontrarse en equipos de red como el FW del aula.
- La función de análisis que permite analizar los paquetes y aportar información.

## Wireshark
Whireshark es un analizador de protocolos para Windows, Linux y OSX. Es una aplicación de escritorio que permite funciones como:

- Captura
- Análisis de protocolos
- Filtros de captura
- Seguir una conversación con un servidor
- Exportación/Importación
- Cálculo de estadísticas

### Consideraciones previas
- Iniciar en modo administrador. 

### Algunos ejemplos
Vamos a ver en clase algunos ejemplos de uso de este programa.

- Capturar todo el tráfico en un interfaz: Captura/Opciones. Revisar todas las opciones de captura.
- Configurar filtros de captura predeterminados: Captura/Filtros de captura
- Exportar/Importar una captura de tráfico.
### Enlaces de interés

- Filtros de captura: https://wiki.wireshark.org/CaptureFilters#capture-filter-is-not-a-display-filter
- Filtros de pantalla: https://wiki.wireshark.org/DisplayFilters

## tcpdump
Es una herramienta para línea de comandos que permite analizar el tráfico en una red. Podemos generar ficheros de captura y luego importarlos por ejemplo en Wireshark para analizarlos.

### Ejemplos de uso (Wikipedia)

Capturar tráfico cuya dirección IP de origen sea 192.168.3.1

    tcpdump src host 192.168.3.1

Capturar tráfico cuya dirección origen o destino sea 192.168.3.2

    tcpdump host 192.168.3.2

Capturar tráfico con destino a la dirección MAC 50:43:A5:AE:69:55

    tcpdump ether dst 50:43:A5:AE:69:55

Capturar tráfico con red destino 192.168.3.0

    tcpdump dst net 192.168.3.0

Capturar tráfico con red origen 192.168.3.0/28

    tcpdump src net 192.168.3.0 mask 255.255.255.240
    tcpdump src net 192.168.3.0/28

Capturar tráfico con destino el puerto 23

    tcpdump dst port 23

Capturar tráfico con origen o destino el puerto 110

    tcpdump port 110

Capturar los paquetes de tipo ICMP

    tcpdump ip proto \\icmp

Capturar los paquetes de tipo UDP

    tcpdump ip proto \\udp
    tcpdump udp

Capturar el tráfico Web

    tcpdump tcp and port 80

Capturar las peticiones de DNS

    tcpdump udp and dst port 53

Capturar el tráfico al puerto telnet o SSH

    tcpdump tcp and \(port 22 or port 23\)

Capturar todo el tráfico excepto el web

    tcpdump tcp and not port 80

otra forma:

    tcpdump tcp and ! port 80

### Enlaces de interés

- Página del manual: https://linux.die.net/man/8/tcpdump


