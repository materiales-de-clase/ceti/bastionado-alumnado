# El nivel 7
En mucho textos y herramientas, cuando se hace referencia a la capa de aplicación se hace referencia a ella como nivel 7. En clase hemos visto los modelos TCP/IP e OSI y los hemos comparado y, como vimos en clase, las funciones asignadas a cada capa así como el número de capas puede diferir de un modelo a otro. Un caso claro es la capa de aplicación, que en el modelo OSI está en el nivel 7 y en el modelo TCP/IP está en el 4. De cualquier modo, generalmente, veréis referencias a la capa de aplicación como nivel 7 o capa de aplicación. 

En este nivel tenemos la implementación de multitud de protocolos. Algunos de ellos ya los habéis estudiado a lo largo de la unidad de trabajo. Por ejemplo:

- HTTP
- SMTP
- Telnet
- FTP
- DNS
- RIP
- SNMP

Son servicios que se apoyan en el resto de capas para dar servicios de aplicación.

