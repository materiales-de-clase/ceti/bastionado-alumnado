# Tarea 01
A continuación tenéis algunos ejercicios de conversión de base numérica. Hay ejercicios para que convirtáis entre las bases binario, octal, decimal y hexadecimal. En la resolución de los ejercicios puedes utilizar la calculadora de windows en modo programador si así lo deseas. 

Resuelve los ejercicios en un documento donde debes pegar también los enunciados. Convierte tus respuesta en un documento PDF y adjúntalo a la tarea.

Como nombre del archivo a adjuntar utiliza en minúsculas el siguiente nombre:

- apellidos_nombre_sistemas_numeracion_tarea01.pdf



### Ejercicio 01 
Pasar al sistema octal, decimal y hexadecimal el número 101111

### Ejercicio 02
Pasar el número 27025 a binario, octal y hexadecimal

### Ejercicio 03
Realiza las siguientes operaciones
- 101101 AND 1011
- 10001 AND 111

### Ejercicio 04
Pasa a binario, octal y decimal el número 3CB

### Ejercicio 05
Pasa a hexadecimal, binario, octal el número 381

### Ejercicio 06
Conversión de binario a decimal, hexadecimal y octal:

- 101110
- 000011
- 101010
- 111000

### Ejercicio 07
Conversión de decimal a binario, hexadecimal y octal

- 64
- 63
- 145
- 500
- 111
- 127
- 128
- 16
- 15
- 8
- 16
- 15
- 255
- 254

### Ejercicio 08
Convertir los siguientes números octales a decimales, hexadecimales y binarios

- 42
- 376
- 11
- 37

### Ejercicio 09
Convertir los siguientes números decimales a sus octales, hexadecimales y binarios equivalentes

- 77375
- 20515625
- 815625
- 445625

### Ejercicio 10
Convertir los siguientes números octales a sus binarios, octales y  hexadecimales equivalentes

- 7
- 16
- 20
- 37

### Ejercicio 11
Convertir los siguientes números binarios a sus equivalentes octales

- 001
- 110
- 111000
- 101100

### Ejercicio 12
Convertir los siguientes números hexadecimales a sus decimales equivalentes:

- F4
- D3E
- 11111
- EBAC
- CAFE
- CACAFEA
- FF
- F

### Ejercicio 13
Convertir los siguientes nº decimales a sus hexadecimales equivalentes

- 204125
- 255875
- 63125
- 10000

### Ejercicio 14
Convertir los siguientes números hexadecimales a sus equivalentes binarios:

- B
- 1C
- 1F
- 239

### Ejercicio 15
Convertir los siguientes números binarios a sus hexadecimales equivalentes:

- 1001
- 110101
- 10000
- 10000000

### Ejercicio 16
Convertir los siguientes hexadecimales a sus decimales
equivalentes:

- C
- 9F
- D52
- 67E
- ABCD